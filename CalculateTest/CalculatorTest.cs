﻿using System;
using LibCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using LibCommand;
using Commands;

namespace CalculateTest
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void RemoveEmpty()
        {
            string input = " 3 + 2.5 - (4 * 6) ";
            Calculator calculator = new Calculator(new List<ICommand>());
            string result = calculator.RemoveEmpty(input);

            Assert.AreEqual(result, "3+2.5-(4*6)", "Очистка проведена некорректно");
        }

        [TestMethod]
        public void ConvertStringToQueue()
        {
            string input = "34+41,5*2/(1-5)";
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number());
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            List<ICommand> testResult = calculator.ConvertStringToList(input, out ErrorMessage);

            List<ICommand> correctResult = new List<ICommand>();
            correctResult.Add(new Number("34"));
            correctResult.Add(new Number("41,5"));
            correctResult.Add(new Number("2"));
            correctResult.Add(new Multiplication());
            correctResult.Add(new Number("1"));
            correctResult.Add(new Number("5"));
            correctResult.Add(new Minus());
            correctResult.Add(new Division());
            correctResult.Add(new Plus());

            Assert.AreEqual(testResult.Count, correctResult.Count, "Длины стеков несовпадают");
            Assert.AreEqual(ErrorMessage, string.Empty, "Поступило уведомление об ошибке");
            if (correctResult.Count == testResult.Count)
            {
                for (int index = 0; index < correctResult.Count; index++)
                {
                    ICommand oneCorrect = correctResult[index];
                    ICommand oneTest = testResult[index];
                    Assert.AreEqual(oneCorrect.TypeC, oneTest.TypeC, "Несовпадение команд по типу");
                    Assert.AreEqual(oneCorrect.Value, oneTest.Value, "Несовпадение команд по значение");
                }
            }
        }

        [TestMethod]
        public void CalculateFromList()
        {
            List<ICommand> input = new List<ICommand>();
            input.Add(new Number("34"));
            input.Add(new Number("41,5"));
            input.Add(new Number("2"));
            input.Add(new Multiplication());
            input.Add(new Number("1"));
            input.Add(new Number("5"));
            input.Add(new Minus());
            input.Add(new Division());
            input.Add(new Plus());

            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Division());
            commands.Add(new Number());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.CalculateFromList(input, out ErrorMessage);

            Assert.AreEqual(result, 13.25, "Неверный рассчет");
        }

        [TestMethod]
        public void Calculate1()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Number());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.Calculate("3.5 + 4 * 2 / (1 - 5)", out ErrorMessage);

            Assert.AreEqual(result, 1.5, "Неверный расчет");
        }
        [TestMethod]
        public void Calculate2()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Number());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.Calculate("((3.5 + 43) * 2) / 2 - 5.1", out ErrorMessage);

            Assert.AreEqual(result, 41.4, "Неверный расчет");
        }
        [TestMethod]
        public void Calculate3()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Number());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.Calculate(" ( - 1 + 3 ) * 5", out ErrorMessage);

            Assert.AreEqual(result, 10, "Неверный расчет " + ErrorMessage);
            Assert.AreEqual(ErrorMessage, string.Empty, "Cообщение об ошибке");
        }
        [TestMethod]
        public void CalculateIncorrect()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Number());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.Calculate(" kkll ", out ErrorMessage);

            Assert.AreEqual(result, 0, "Неверный расчет " + ErrorMessage);
            Assert.AreNotEqual(ErrorMessage, string.Empty, "Не отображение ошибочного значения");
        }
        [TestMethod]
        public void Calculate4()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Number());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string ErrorMessage;
            double result = calculator.Calculate("-4", out ErrorMessage);

            Assert.AreEqual(result, -4, "Неверный расчет" + ErrorMessage);
            Assert.AreEqual(ErrorMessage, string.Empty, "Неверное сообщение об ошибке");
        }
    }
}
