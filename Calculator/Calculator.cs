﻿using LibCommand;
using LibLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Библиотека реализации калькулятора (без команд - операций)
/// </summary>
namespace LibCalculator
{
    public class Calculator
    {
        /// <summary>
        /// Список операций, которые можно использовать для работы
        /// </summary>
        public IEnumerable<ICommand> Commands
        {
            get { return commands; }
            set
            {
                // Первым в очереди ставим Number. Тогда в задачах типа -3 + 2 первое -3 будет правильно прочитано, как число.
                commands = value.Where(c => c.TypeC == TypeCommand.Number).Concat(value.Where(c => c.TypeC != TypeCommand.Number));
            }
        }
        protected IEnumerable<ICommand> commands = null;

        /// <summary>
        /// Исходная строка 
        /// </summary>
        protected string Input { get; set; }

        /// <summary>
        /// Логирование
        /// </summary>
        public ILog Log { get; set; }

        public Calculator(IEnumerable<ICommand> commands)
        {
            Commands = commands;
        }

        /// <summary>
        /// Вычисление значения по строке
        /// </summary>
        /// <param name="ErrorMessage">Сообщение об ошибке</param>
        /// <returns>Вычисленное значение</returns>
        public double Calculate(string Input, out string ErrorMessage)
        {
            ErrorMessage = "";
            double result = 0;
            string tempInput = this.RemoveEmpty(Input);
            List<ICommand> collectionInput = this.ConvertStringToList(tempInput, out ErrorMessage);
            if (string.IsNullOrWhiteSpace(ErrorMessage))
            {
                result = this.CalculateFromList(collectionInput, out ErrorMessage);
            }
            return result;
        }

        /// <summary>
        /// Удаление всех пробелов
        /// </summary>
        /// <param name="Input">Входная строка</param>
        /// <returns>Очищенная строка</returns>
        public string RemoveEmpty(string Input)
        {
            return Input.Trim().Replace(" ", "");
        }

        /// <summary>
        /// Преобразование строки в обратную польскую запись
        /// </summary>
        /// <param name="Input">Входная строка</param>
        /// <param name="ErrorMessage">Сообщение об ошибке</param>
        /// <returns>Обратная польская запись</returns>
        public List<ICommand> ConvertStringToList(string Input, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            List<ICommand> result = new List<ICommand>();

            try
            {
                // Рабочая строка, которую разбираем и обрезаем по мере разбора
                string workInput = Input;
                Stack<ICommand> stack = new Stack<ICommand>();
                ICommand lastCommand = null;
                while (workInput.Length > 0)
                {
                    ICommand correctCommand = null;
                    string commandText = "";
                    // Подряд не могут идти два числа, поэтому если предыдущее было число, то сейчас его не ищем
                    if (lastCommand?.TypeC == TypeCommand.Number)
                    {
                        correctCommand = this.Commands.FirstOrDefault(c => c.IsCommand(workInput, out commandText) && c.TypeC != TypeCommand.Number);
                    }
                    else
                    {
                        correctCommand = this.Commands.FirstOrDefault(c => c.IsCommand(workInput, out commandText));
                    }
                    if (correctCommand != null)
                    {
                        // Команда это операция или скобка - сохраняем
                        workInput = workInput.Substring(commandText.Length);
                        switch (correctCommand.TypeC)
                        {
                            case TypeCommand.Number:
                                result.Add(correctCommand.Create(commandText));
                                break;
                            case TypeCommand.BracketOpen:
                                stack.Push(correctCommand.Create(commandText));
                                break;
                            case TypeCommand.BracketClose:
                                try
                                {
                                    while (stack.Peek().TypeC != TypeCommand.BracketOpen)
                                    {
                                        result.Add(stack.Pop());
                                    }
                                    if (stack.Peek().TypeC == TypeCommand.BracketOpen)
                                    {
                                        stack.Pop();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorMessage = LibCalculator.Properties.Resources.bracketCountIncorrect;
                                    workInput = "";
                                }
                                break;
                            case TypeCommand.BinaryOperation:
                                while (stack.Count() > 0 && stack.Peek().TypeC == TypeCommand.BinaryOperation && stack.Peek().Priority >= correctCommand.Priority)
                                {
                                    result.Add(stack.Pop());
                                }
                                stack.Push(correctCommand);
                                break;
                        }
                    }
                    else
                    {
                        ErrorMessage = LibCalculator.Properties.Resources.commandsIncorrect;
                        workInput = "";
                        stack.Clear();
                        result.Clear();
                    }
                    lastCommand = correctCommand;
                }
                if (string.IsNullOrWhiteSpace(ErrorMessage))
                {
                    while (stack.Count > 0)
                    {
                        result.Add(stack.Pop());
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrWhiteSpace(ErrorMessage))
                {
                    ErrorMessage = LibCalculator.Properties.Resources.errorAny;
                    result = new List<ICommand>();
                }
            }
            return result;
        }

        public double CalculateFromList(List<ICommand> listCommand, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            double result = 0;
            try
            {
                if (listCommand.Count > 1)
                {
                    while (listCommand.Count > 1)
                    {
                        ICommand command = null;
                        for (int index = 0; index < listCommand.Count; index++)
                        {
                            if (listCommand[index].TypeC == TypeCommand.BinaryOperation)
                            {
                                command = listCommand[index];
                                double temp = command.Calculate(listCommand, index, out ErrorMessage);
                                if (!string.IsNullOrWhiteSpace(ErrorMessage))
                                {
                                    result = 0;
                                    listCommand = new List<ICommand>();
                                    break;
                                }
                                if (listCommand.Count == 1)
                                {
                                    result = temp;
                                }
                                break;
                            }
                        }
                        if (listCommand.Count > 1 && command == null)
                        {
                            ErrorMessage = LibCalculator.Properties.Resources.calculateNot;
                            break;
                        }
                    }
                }
                else if(listCommand.Count==1 && listCommand[0].TypeC== TypeCommand.Number)
                {
                    double.TryParse(listCommand[0].Value.ToString(), out result);
                }
                else
                {
                    ErrorMessage = LibCalculator.Properties.Resources.calculateNot;
                }
            }
            catch (Exception ex) { ErrorMessage = LibCalculator.Properties.Resources.errorAny; result = 0; }
            return result;
        }
    }
}
