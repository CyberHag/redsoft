﻿using LibCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public class BracketOpen : ICommand
    {
        public TypeCommand TypeC { get { return TypeCommand.BracketOpen; } }

        public int Priority { get { return 0; } }
        public object Value { get { return value; } }
        protected string value = string.Empty;

        public BracketOpen() { }
        public BracketOpen(string NewValue) { value = NewValue; }

        public double Calculate(List<ICommand> commands, int indexCommand, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            return 0;
        }

        public bool IsCommand(string Input, out string CommandText)
        {
            CommandText = "(";
            bool result = Input.Substring(0, Math.Min(Input.Length, CommandText.Length)) == CommandText;
            if (result) value = Input; else value = null;
            return result;
        }

        public ICommand Create(string NewValue)
        {
            return new BracketOpen(NewValue);
        }
    }
}
