﻿using LibCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public class Division : ICommand
    {
        public TypeCommand TypeC { get { return TypeCommand.BinaryOperation; } }

        public int Priority { get { return 2; } }
        public object Value { get { return null; } }

        public double Calculate(List<ICommand> commands, int indexCommand, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            if (commands.Count < 3)
            {
                ErrorMessage = Commands.Properties.Resources.argumentCountIncorrect;
                return 0;
            }
            ICommand c1 = null;
            ICommand c2 = null;
            try
            {
                c1 = commands[indexCommand - 2];
                c2 = commands[indexCommand - 1];
            }
            catch
            {
                ErrorMessage = Commands.Properties.Resources.argumentCountIncorrect;
                return 0;
            }
            double arg1;
            double arg2;
            try
            {
                arg1 = (double)c1.Value;
                arg2 = (double)c2.Value;
            }
            catch
            {
                ErrorMessage = string.Format(Commands.Properties.Resources.argumentNotNumber, c1.Value, c2.Value);
                return 0;
            }
            commands.RemoveRange(indexCommand - 2, 3);
            double result = arg1 / arg2;
            commands.Insert(indexCommand - 2, new Number(result.ToString()));
            return result;
        }

        public bool IsCommand(string Input,out string CommandText)
        {
            CommandText= "/";
            bool result = Input.Substring(0, Math.Min(Input.Length, CommandText.Length)) == CommandText;
            return result;
        }

        public ICommand Create(string NewValue)
        {
            return new Division();
        }
    }
}
