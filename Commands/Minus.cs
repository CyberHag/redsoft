﻿using LibCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public class Minus:ICommand
    {
        public TypeCommand TypeC { get { return TypeCommand.BinaryOperation; } }

        public int Priority { get { return 1; } }
        public object Value { get { return null; } }

        public double Calculate(List<ICommand> commands, int indexCommand, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            if (commands.Count < 2)
            {
                ErrorMessage = Commands.Properties.Resources.argumentCountIncorrect;
                return 0;
            }
            ICommand c1 = null;
            ICommand c2 = null;
            try
            {
                // Если неудается прочесть первый аргумент, то предполагаем, что это было отрицательное число
                try { c1 = commands[indexCommand - 2]; } catch { c1 = new Number("0"); }
                if(c1.TypeC!= TypeCommand.Number) { c1 = new Number("0"); }
                c2 = commands[indexCommand-1];
            }
            catch
            {
                ErrorMessage = Commands.Properties.Resources.argumentCountIncorrect;
                return 0;
            }
            double arg1;
            double arg2;
            try
            {
                arg1 = (double)c1.Value;
                arg2 = (double)c2.Value;
            }
            catch
            {
                ErrorMessage = string.Format(Commands.Properties.Resources.argumentNotNumber, c1.Value, c2.Value);
                return 0;
            }
            double result = arg1 - arg2;
            if (indexCommand - 2 >= 0 && commands[indexCommand - 2].TypeC == TypeCommand.Number)
            {
                commands.RemoveRange(indexCommand - 2, 3);
                commands.Insert(indexCommand - 2, new Number(result.ToString()));
            }
            else
            {
                commands.RemoveRange(indexCommand - 1, 2);
                commands.Insert(indexCommand - 1, new Number(result.ToString()));
            }
            return result;
        }

        public bool IsCommand(string Input, out string CommandText)
        {
            CommandText = "-";
            bool result = Input.Substring(0, Math.Min(Input.Length, CommandText.Length)) == CommandText;
            return result;
        }

        public ICommand Create(string NewValue)
        {
            return new Minus();
        }
    }
}
