﻿using LibCommand;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Commands
{
    public class Number : ICommand
    {
        public TypeCommand TypeC { get { return TypeCommand.Number; } }

        public int Priority { get { return 0; } }

        public object Value { get { return value; } }
        protected double? value = null;

        NumberStyles styles = NumberStyles.Any;
        CultureInfo provider = new CultureInfo("ru-Ru");

        public double Calculate(List<ICommand> commands, int indexCommand, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            return value ?? 0;
        }

        public bool IsCommand(string Input, out string CommandText)
        {
            //Regex reg = new Regex("^[-]?[0-9]+[.,]?[0-9]*$");
            Regex reg = new Regex("^[-]?[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?");
            Match result = reg.Match(Input);
            if (result.Success)
            {
                CommandText = result.Value;
                return true;
            }
            else
            {
                CommandText = "";
                return false;
            }
        }

        public Number(string NewValue)
        {
            // Независимо от региональных настроек работаем с русским вариантом представления числа
            value = Double.Parse(NewValue.Replace('.', ','), styles, provider);
        }

        public Number() { }

        public ICommand Create(string NewValue)
        {
            return new Number(NewValue);
        }
    }
}
