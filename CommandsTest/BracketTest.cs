﻿using System;
using Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommandsTest
{
    [TestClass]
    public class BracketTest
    {
        [TestMethod]
        public void IsCommandCorrect1()
        {
            string input = "(";
            BracketOpen bracket = new BracketOpen();
            string commandText;
            bool result = bracket.IsCommand(input, out commandText);

            Assert.AreEqual(result, true, "Ошибка распознования команды");
        }

        [TestMethod]
        public void IsCommandCorrect2()
        {
            string input = ")";
            BracketOpen bracket = new BracketOpen();
            string commandText;
            bool result = bracket.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка распознования команды");
        }

        [TestMethod]
        public void IsCommandUnCorrect1()
        {
            string input = "скобка";
            BracketOpen bracket = new BracketOpen();
            string commandText;
            bool result = bracket.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка распознования команды");
        }



        public void IsCommandCorrect3()
        {
            string input = "(";
            BracketClose bracket = new BracketClose();
            string commandText;
            bool result = bracket.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка распознования команды");
        }

        [TestMethod]
        public void IsCommandCorrect4()
        {
            string input = ")";
            BracketClose bracket = new BracketClose();
            string commandText;
            bool result = bracket.IsCommand(input,out commandText);

            Assert.AreEqual(result, true, "Ошибка распознования команды");
        }

        [TestMethod]
        public void IsCommandUnCorrec2t()
        {
            string input = "скобка";
            BracketClose bracket = new BracketClose();
            string commandText;
            bool result = bracket.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка распознования команды");
        }
    }
}
