﻿using System;
using System.Collections.Generic;
using Commands;
using LibCommand;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommandsTest
{
    [TestClass]
    public class DivisionTest
    {
        [TestMethod]
        public void IsCommandCorrect()
        {
            string input = "/";
            Division division = new Division();
            string commandText;
            bool result = division.IsCommand(input,out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на правильную команду");
        }

        [TestMethod]
        public void IsCommandFalse()
        {
            string input = "частное";
            Division division = new Division();
            string commandText;
            bool result = division.IsCommand(input, out commandText);

            Assert.AreEqual(result, false, "Ошибка проверки на неверную команду");
        }

        [TestMethod]
        public void CalculateCorrect()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Number("25"));
            commands.Add(new Number("-4"));
            Division division = new Division();
            commands.Add(division);
            commands.Add(new Number("700"));
            string err;
            double result = division.Calculate(commands, 3, out err);

            Assert.AreEqual(result, -6.25, "Ошибка вычисления команды");
            Assert.AreEqual(commands.Count, 3, "Неверно почищена очередь");
            Assert.AreEqual(commands[1].Value, -6.25, "Неверный порядок в очереди");
            Assert.AreEqual(err, string.Empty, "Некорректное возвращение сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateNotNumber()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Division());
            commands.Add(new Number("-4"));
            Division division = new Division();
            commands.Add(division);
            string err;
            double result = division.Calculate(commands,3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateIncorrectCountArgument()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Division());
            commands.Add(new Number("-4"));
            Division division = new Division();
            commands.Add(division);
            string err;
            double result = division.Calculate(commands,3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }
    }
}
