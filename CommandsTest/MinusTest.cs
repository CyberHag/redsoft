﻿using System;
using System.Collections.Generic;
using Commands;
using LibCommand;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommandsTest
{
    [TestClass]
    public class MinusTest
    {
        [TestMethod]
        public void IsCommandCorrect()
        {
            string input = "-";
            Minus minus = new Minus();
            string commandText;
            bool result = minus.IsCommand(input,out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на правильную команду");
        }

        [TestMethod]
        public void IsCommandFalse()
        {
            string input = "минус";
            Minus minus = new Minus();
            string commandText;
            bool result = minus.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка проверки на неверную команду");
        }

        [TestMethod]
        public void CalculateCorrect()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Number("25"));
            commands.Add(new Number("-4"));
            Minus minus = new Minus();
            commands.Add(minus);
            commands.Add(new Number("1000"));
            string err;
            double result = minus.Calculate(commands, 3, out err);

            Assert.AreEqual(result, 29, "Ошибка вычисления команды");
            Assert.AreEqual(commands.Count, 3, "Неверно почищена очередь");
            Assert.AreEqual((double)commands[1].Value, 29, "Неверный порядок в очереди");
            Assert.AreEqual(err, string.Empty, "Некорректное возвращение сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateNotNumber()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Number("-4"));
            commands.Add(new Minus());
            Minus minus = new Minus();
            commands.Add(minus);
            string err;
            double result = minus.Calculate(commands,3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateIncorrectCountArgument()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Number("-4"));
            commands.Add(new Minus());
            Minus minus = new Minus();
            commands.Add(minus);
            string err;
            double result = minus.Calculate(commands, 3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }
    }
}
