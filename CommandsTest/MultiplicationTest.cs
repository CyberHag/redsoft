﻿using System;
using System.Collections.Generic;
using Commands;
using LibCommand;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommandsTest
{
    [TestClass]
    public class MultiplicationTest
    {
        [TestMethod]
        public void IsCommandCorrect()
        {
            string input = "*";
            Multiplication multiplication = new Multiplication();
            string commandText;
            bool result = multiplication.IsCommand(input, out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на правильную команду");
        }

        [TestMethod]
        public void IsCommandFalse()
        {
            string input = "произведение";
            Multiplication multiplication = new Multiplication();
            string commandText;
            bool result = multiplication.IsCommand(input,out commandText);

            Assert.AreEqual(result, false, "Ошибка проверки на неверную команду");
        }

        [TestMethod]
        public void CalculateCorrect()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Number("25"));
            commands.Add(new Number("-4"));
            Multiplication multiplication = new Multiplication();
            commands.Add(multiplication);
            commands.Add(new Number("1000"));
            string err;
            double result = multiplication.Calculate(commands, 3, out err);

            Assert.AreEqual(result, -100, "Ошибка вычисления команды");
            Assert.AreEqual(commands.Count, 3, "Неверно почищена очередь");
            Assert.AreEqual((double)commands[1].Value, -100, "Неверный порядок в очереди");
            Assert.AreEqual(err, string.Empty, "Некорректное возвращение сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateNotNumber()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Multiplication());
            commands.Add(new Number("-4"));
            Multiplication multiplication = new Multiplication();
            commands.Add(multiplication);
            string err;
            double result = multiplication.Calculate(commands, 3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }

        [TestMethod]
        public void CalculateIncorrectCountArgument()
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number("500"));
            commands.Add(new Multiplication());
            commands.Add(new Number("-4"));
            Multiplication multiplication = new Multiplication();
            commands.Add(multiplication);
            string err;
            double result = multiplication.Calculate(commands,3, out err);

            Assert.AreEqual(result, 0, "Ошибка вычисления команды");
            Assert.AreNotEqual(err, string.Empty, "Не возвращено сообщения об ошибке");
        }
    }
}
