﻿using System;
using Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommandsTest
{
    [TestClass]
    public class NumberTest
    {
        [TestMethod]
        public void IsCommandTrueWithComma()
        {
            string input = "25,6e3+";
            Number number = new Number();
            string commandText;
            bool result = number.IsCommand(input, out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на правильное число с запятой");
        }

        [TestMethod]
        public void IsCommandTrueWithPoint()
        {
            string input = "-25.6e-2-";
            Number number = new Number();
            string commandText;
            bool result = number.IsCommand(input,out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на правильное число с точкой");
        }

        [TestMethod]
        public void IsCommandCorrect3()
        {
            string input = "3+";
            Number number = new Number();
            string commandText;
            bool result = number.IsCommand(input, out commandText);

            Assert.AreEqual(result, true, "Ошибка проверки на верное число");
        }

        [TestMethod]
        public void IsCommandFalse()
        {
            string input = "*38.4";
            Number number = new Number();
            string commandText;
            bool result = number.IsCommand(input, out commandText);

            Assert.AreEqual(result, false, "Ошибка проверки на неверное число");
        }
    }
}
