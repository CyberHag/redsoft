﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCommand
{
    public interface ICommand
    {
        /// <summary>
        /// Тип команды
        /// </summary>
        TypeCommand TypeC { get;  }

        /// <summary>
        /// Приоритет команды
        /// </summary>
        int Priority { get; }

        /// <summary>
        /// Проверяет является ли начало строки командой
        /// </summary>
        /// <param name="Input">Строка для проверки</param>
        /// <param name="CommandText">Текст команды</param>
        /// <returns></returns>
        bool IsCommand(string Input, out string CommandText);

        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <param name="commands">Очередь команд и значений</param>
        /// <returns>вычисленное значение</returns>
        double Calculate(List<ICommand> commands, int indexCommand, out string ErrorMessage);

        /// <summary>
        /// Значение числа (если команда это число) или открывающая/закрывающая скобка (если команда это скобка)
        /// </summary>
        object Value { get; }

        /// <summary>
        /// Создает субъект такого же класса с указанным значением
        /// </summary>
        /// <param name="NewValue"></param>
        /// <returns></returns>
        ICommand Create(string NewValue);
    }
}
