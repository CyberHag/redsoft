﻿/// <summary>
/// Библиотека связи с логирование
/// </summary>
namespace LibLog
{
    public interface ILog
    {
        /// <summary>
        /// Свойство указывающее используется режим отладки или нет
        /// </summary>
        bool IsDebug { get; set; }

        /// <summary>
        /// Сохранение отладочной информации
        /// </summary>
        /// <param name="Message">Текстовое сообщение</param>
        void WriteDebug(string Message);

        /// <summary>
        /// Сохранение информации предупреждения (штатная работа)
        /// </summary>
        /// <param name="Message">Текстовое сообщение</param>
        void WriteWarn(string Message);

        /// <summary>
        /// Сохранение информации об ошибке в работе приложения
        /// </summary>
        /// <param name="Message">Текстовое сообщение</param>
        void WriteError(string Message);
    }
}
