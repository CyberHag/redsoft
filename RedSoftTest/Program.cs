﻿using Commands;
using LibCalculator;
using LibCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedSoftTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ICommand> commands = new List<ICommand>();
            commands.Add(new Number());
            commands.Add(new Plus());
            commands.Add(new Minus());
            commands.Add(new Multiplication());
            commands.Add(new Division());
            commands.Add(new BracketOpen());
            commands.Add(new BracketClose());
            Calculator calculator = new Calculator(commands);
            string errorMessage;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            while (true)
            {
                Console.WriteLine("Input string for calculate");
                string input = Console.ReadLine();
                errorMessage = "";
                double result = calculator.Calculate(input, out errorMessage);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    Console.WriteLine(errorMessage);
                }
                else
                {
                    Console.WriteLine(result);
                }
            }
        }
    }
}
